package com.tsc.skuschenko.tm.command.data;


import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "save data in yaml file";

    @NotNull
    private static final String NAME = "data-save-yaml";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        serviceLocator.getDataEndpoint().saveDataYamlFasterXmlCommand(session);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String[] roles() {
        return new String[]{"Administrator"};
    }

}
