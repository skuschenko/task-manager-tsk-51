package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "clear all projects";

    @NotNull
    private static final String NAME = "project-clear";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        serviceLocator.getProjectEndpoint().clearProjects(session);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
