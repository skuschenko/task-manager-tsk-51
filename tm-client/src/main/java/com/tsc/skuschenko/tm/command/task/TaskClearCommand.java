package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "clear all tasks";

    @NotNull
    private static final String NAME = "task-clear";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        serviceLocator.getTaskEndpoint().clearTask(session);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
