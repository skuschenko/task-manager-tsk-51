package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.repository.*;
import com.tsc.skuschenko.tm.api.service.*;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.endpoint.*;
import com.tsc.skuschenko.tm.exception.system.UnknownArgumentException;
import com.tsc.skuschenko.tm.exception.system.UnknownCommandException;
import com.tsc.skuschenko.tm.repository.*;
import com.tsc.skuschenko.tm.service.*;
import com.tsc.skuschenko.tm.util.SystemUtil;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String COMMAND_PATH = "com.tsc.skuschenko.tm.command";

    @NotNull
    private static final String OPERATION_FAIL = "fail";

    @NotNull
    private static final String OPERATION_OK = "ok";

    @NotNull
    private static final String TM_PID = "task-manager.pid";

    @Nullable private Session session;

    @NotNull
    private final ICommandRepository commandRepository =
            new CommandRepository();

    @NotNull
    private final ICommandService commandService =
            new CommandService(commandRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final ProjectEndpointService projectEndpointService =
            new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint =
            projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final TaskEndpointService taskEndpointService
            = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint =
            taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService
            = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint =
            userEndpointService.getUserEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService =
            new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint =
            sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final DataEndpointService dataEndpointService =
            new DataEndpointService();

    @NotNull
    private final DataEndpoint dataEndpoint  =
            dataEndpointService.getDataEndpointPort();

    private void createDefaultProjectAndTask(
            @NotNull final Session session, @NotNull final String name,
            @NotNull String description
    ) {
                projectEndpoint.addProject(session, name, description);
                taskEndpoint.addTask(session, name, description);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    private void init() {
        initCommands();
     //  initUsers();
        initPID();
    }

    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections(COMMAND_PATH);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        classes.stream().filter(item ->
                !Modifier.isAbstract(item.getModifiers()))
                .sorted(Comparator.comparing(Class::getName))
                .forEach(item -> {
                    try {
                        registry(item.newInstance());
                    } catch (InstantiationException |
                            IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = TM_PID;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
       @NotNull final Session session =
               sessionEndpoint.openSession("admin","admin");
                userEndpoint.createUserWithEmail(
                        "test1", "test1", "text1@test.ru"
                );
        userEndpoint.createUserWithRole("admin1", "admin1", "ADMIN");
        sessionEndpoint.closeSession(session);
        @NotNull final Session sessionTest =
                sessionEndpoint.openSession("test1","test1");
        sessionEndpoint.closeSession(sessionTest);
        createDefaultProjectAndTask(sessionTest, "Test1", "Test1");
        @NotNull final Session sessionAdmin =
                sessionEndpoint.openSession("admin1","admin1");
        createDefaultProjectAndTask(sessionAdmin, "Admin1", "Admin1");
        sessionEndpoint.closeSession(sessionAdmin);
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        @Nullable final AbstractCommand abstractCommand
                = commandService.getCommandByArg(arg);
        Optional.ofNullable(abstractCommand)
                .orElseThrow(() -> new UnknownArgumentException(arg));
        abstractCommand.execute();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (!Optional.ofNullable(args).filter(item -> item.length > 1)
                .isPresent()) {
            return false;
        }
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        @Nullable final AbstractCommand abstractCommand
                = commandService.getCommandByName(command);
        Optional.ofNullable(abstractCommand)
                .orElseThrow(() -> new UnknownCommandException(command));
        abstractCommand.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String... args) {
        logService.debug("TEST");
        logService.info("***Welcome to task manager***");
        init();
        if (parseArgs(args)) exit();
        while (true) {
            try {
                System.out.print("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("[" + OPERATION_OK.toUpperCase() + "]");
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println("[" + OPERATION_FAIL.toUpperCase() + "]");
            }
        }
    }

}
