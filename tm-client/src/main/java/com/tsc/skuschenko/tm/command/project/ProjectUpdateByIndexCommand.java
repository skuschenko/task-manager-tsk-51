package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.endpoint.Project;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "update project by index";

    @NotNull
    private static final String NAME = "project-update-by-index";

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("index");
        @NotNull final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        Project project = serviceLocator.getProjectEndpoint()
                .findProjectByIndex(session, valueIndex);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showParameterInfo("name");
        @NotNull final String valueName = TerminalUtil.nextLine();
        showParameterInfo("description");
        @NotNull final String valueDescription = TerminalUtil.nextLine();
        project = serviceLocator.getProjectEndpoint().updateProjectByIndex(
                session, valueIndex, valueName, valueDescription
        );
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
