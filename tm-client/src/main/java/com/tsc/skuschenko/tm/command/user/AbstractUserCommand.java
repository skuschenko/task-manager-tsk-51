package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.endpoint.User;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(final @NotNull User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

}
