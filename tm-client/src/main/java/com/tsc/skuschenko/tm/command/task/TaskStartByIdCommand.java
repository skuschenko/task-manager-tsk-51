package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "start task by id";

    @NotNull
    private static final String NAME = "task-start-by-id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("id");
        @NotNull final String value = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint()
                .startTaskById(session, value);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
