package com.tsc.skuschenko.tm.exception.system;

import com.tsc.skuschenko.tm.exception.AbstractException;
import org.jetbrains.annotations.Nullable;

public final class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument does not found...");
    }

    public UnknownArgumentException(@Nullable final String argument) {
        super("Error! Argument '" + argument + "' does not found...");
        System.out.println("Pleas print 'help' for more information");
    }

}
