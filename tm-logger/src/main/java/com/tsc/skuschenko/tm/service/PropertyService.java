package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String ENTITIES = "entityForLog";

    @NotNull
    private static final String FILE_PATH_PROJECT = "filePath.project";

    @NotNull
    private static final String FILE_PATH_SESSION = "filePath.session";

    @NotNull
    private static final String FILE_PATH_TASK = "filePath.task";

    @NotNull
    private static final String FILE_PATH_USER = "filePath.user";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Nullable
    @Override
    public String getFilePath(@Nullable final String className) {
        if (!Optional.ofNullable(className).isPresent()) return null;
        switch (className) {
            case "SessionDTO":
            case "Session":
                return properties.getProperty(FILE_PATH_SESSION);
            case "TaskDTO":
            case "Task":
                return properties.getProperty(FILE_PATH_TASK);
            case "UserDTO":
            case "User":
                return properties.getProperty(FILE_PATH_USER);
            case "ProjectDTO":
            case "Project":
                return properties.getProperty(FILE_PATH_PROJECT);
            default:
                return null;
        }
    }

    @Override
    @NotNull
    public List<String> getLogEntities() {
        @Nullable final String entities = properties.getProperty(ENTITIES);
        if (!Optional.ofNullable(entities).isPresent())
            return new ArrayList<>();
        return Arrays.asList(entities.split(","));
    }

}
