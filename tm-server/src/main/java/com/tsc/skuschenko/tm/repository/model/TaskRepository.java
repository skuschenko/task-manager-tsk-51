package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.api.repository.model.ITaskRepository;
import com.tsc.skuschenko.tm.model.Task;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public final class TaskRepository
        extends AbstractRepository<Task>
        implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String query =
                "DELETE FROM TaskDto e WHERE e.userId = :userId";
        entityManager.createQuery(query)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clearAllTasks() {
        @NotNull final String query = "DELETE FROM TaskDto e";
        entityManager.createQuery(query)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        @NotNull final String query = "SELECT e FROM TaskDto e ";
        return entityManager.createQuery(query, Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public @Nullable List<Task> findAllTaskByProjectId(
            @NotNull String userId, @NotNull String projectId
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId AND " +
                        "e.projectId = :projectId";
        return entityManager.createQuery(query, Task.class)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }


    @Nullable
    @Override
    public List<Task> findAllWithUserId(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId";
        return entityManager.createQuery(query, Task.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findById(@NotNull String id) {
        return entityManager.find(Task.class, id);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId AND " +
                        "e.id = :id";
        TypedQuery<Task> typedQuery =
                entityManager.createQuery(query, Task.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public Task findOneByIndex(
            @NotNull String userId, @NotNull Integer index
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId";
        TypedQuery<Task> typedQuery =
                entityManager.createQuery(query, Task.class)
                        .setParameter("userId", userId).
                        setFirstResult(index);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public Task findOneByName(
            @NotNull String userId, @NotNull String name
    ) {
        @NotNull final String query =
                "SELECT e FROM TaskDto e WHERE e.userId = :userId AND " +
                        "e.name = :name";
        TypedQuery<Task> typedQuery =
                entityManager.createQuery(query, Task.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public Task getReference(@NotNull final String id) {
        return entityManager.getReference(Task.class, id);
    }

    @Override
    public void removeOneById(
            @NotNull String userId, @NotNull String projectId
    ) {
        @NotNull final String query =
                "DELETE FROM TaskDto e WHERE e.userId = :userId AND " +
                        "e.projectId = :projectId";
        entityManager.createQuery(query)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}