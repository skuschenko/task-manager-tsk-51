package com.tsc.skuschenko.tm.service.dto;

import com.tsc.skuschenko.tm.api.repository.dto.ITaskDTORepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.dto.ITaskDTOService;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.repository.dto.TaskDTORepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskDTOService implements ITaskDTOService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskDTOService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO add(
            @Nullable final String userId, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final ITaskDTORepository taskRepository =
                new TaskDTORepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@Nullable final List<TaskDTO> tasks) {
        Optional.ofNullable(tasks).ifPresent(
                items -> items.forEach(
                        item -> add(
                                item.getUserId(),
                                item.getName(),
                                item.getDescription())
                )
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setStatus(status.getDisplayName());
            task.setStatus(status.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setStatus(status.getDisplayName());
            task.setStatus(status.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setStatus(status.getDisplayName());
            task.setStatus(status.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;

        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository =
                    new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clearAllTasks();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            entityRepository.clear(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setDateFinish(new Date());
            task.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setDateFinish(new Date());
            task.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;

        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO completeByName(@NotNull final String userId,
                                  @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setDateFinish(new Date());
            task.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            return entityRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<TaskDTO> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {

            return entityRepository.findAllWithUserId(userId)
                    .stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            return entityRepository.findAllWithUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            return entityRepository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            return entityRepository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            return entityRepository.findOneByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            entityRepository.remove(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            entityRepository.remove(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            entityRepository.remove(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setDateFinish(new Date());
            task.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task = Optional.ofNullable(
                    findOneByIndex(userId, index)
            ).orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setDateFinish(new Date());
            task.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task = Optional.ofNullable(
                    findOneByName(userId, name)
            ).orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setDateFinish(new Date());
            task.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task = Optional.ofNullable(
                    findOneById(userId, id)
            ).orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setName(name);
            task.setDescription(description);
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final EntityManager entityManager =
                connectionService.getEntityManager();
        @NotNull final ITaskDTORepository entityRepository =
                new TaskDTORepository(entityManager);
        try {
            @NotNull final TaskDTO task = Optional.ofNullable(
                    findOneByIndex(userId, index)
            ).orElseThrow(TaskNotFoundException::new);
            entityManager.getTransaction().begin();
            task.setName(name);
            task.setDescription(description);
            entityRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}