package com.tsc.skuschenko.tm.exception.entity;

import com.tsc.skuschenko.tm.exception.AbstractException;
import org.jetbrains.annotations.Nullable;

public final class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException(@Nullable final String entity) {
        super("Error! " + entity + " not found...");
    }

}
