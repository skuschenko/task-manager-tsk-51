package com.tsc.skuschenko.tm.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.api.service.IBroadcastService;
import com.tsc.skuschenko.tm.component.JmsMessageComponent;
import com.tsc.skuschenko.tm.dto.AbstractWrapper;
import com.tsc.skuschenko.tm.enumerated.EntityOperationType;
import com.tsc.skuschenko.tm.util.SystemUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter
public class BroadcastService implements IBroadcastService {

    @NotNull
    private final ExecutorService executor = Executors.newFixedThreadPool(3);

    @SneakyThrows
    @NotNull
    private AbstractWrapper getDataForMessage(
            @NotNull final EntityOperationType operationType,
            @NotNull final Object entity
    ) {
        @NotNull AbstractWrapper abstractWrapper = new AbstractWrapper();
        abstractWrapper.setDate(SystemUtil.getCurrentDateTime());
        abstractWrapper.setClassName(entity.getClass().getSimpleName());
        abstractWrapper.setOperation(operationType);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.
                writerWithDefaultPrettyPrinter().
                writeValueAsString(entity);
        abstractWrapper.setEntity(json);
        return abstractWrapper;
    }

    @Override
    public void sendJmsMessageAsync(
            @NotNull final Object entity,
            @NotNull final EntityOperationType operationType
    ) {
        submit(() -> {
            try {
                sendJmsMessageSync(entity,operationType);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void sendJmsMessageSync(
            @NotNull final Object entity,
            @NotNull final EntityOperationType operationType
            ) throws JMSException {
        @NotNull final Connection connection =
                JmsMessageComponent.getInstance().getConnection();
        @NotNull final Session session = connection.createSession(
                false, Session.AUTO_ACKNOWLEDGE
        );
        @NotNull final AbstractWrapper abstractWrapper =
                getDataForMessage(operationType, entity);
        @NotNull final Destination destination =
                session.createTopic(abstractWrapper.getClassName());
        @NotNull MessageProducer messageProducer =
                session.createProducer(destination);
        @NotNull final ObjectMessage textMessage =
                session.createObjectMessage(abstractWrapper);
        messageProducer.send(textMessage);
        messageProducer.close();
        session.close();
    }

    @Override
    public void submit(@NotNull Runnable runnable){
        executor.submit(runnable);
    }

    @Override
    public void shutdown(){
        executor.shutdown();
    }

}
