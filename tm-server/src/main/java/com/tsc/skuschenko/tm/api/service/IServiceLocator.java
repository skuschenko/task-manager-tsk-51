package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.service.dto.*;
import com.tsc.skuschenko.tm.api.service.dto.ITaskService;
import com.tsc.skuschenko.tm.api.service.model.IProjectService;
import com.tsc.skuschenko.tm.api.service.model.IProjectTaskService;
import com.tsc.skuschenko.tm.api.service.model.ISessionService;
import com.tsc.skuschenko.tm.api.service.model.IUserService;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    void exit();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IConnectionService getConnectionService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IProjectDTOService getProjectDTOService();

    IProjectService getProjectService();

    @NotNull
    IProjectTaskDTOService getProjectTaskDTOService();

    IProjectTaskService getProjectTaskService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionDTOService getSessionDTOService();

    ISessionService getSessionService();

    @NotNull
    ITaskDTOService getTaskDTOService();

    ITaskService getTaskService();

    @NotNull
    IUserDTOService getUserDTOService();

    IUserService getUserService();
}
