package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.api.service.dto.IProjectDTOService;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.enumerated.Sort;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint {

    @Nullable IProjectDTOService projectService;

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectDTOService();
    }

    @WebMethod
    @NotNull
    public ProjectDTO addProject(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description")
            @NotNull final String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.add(session.getUserId(), name, description);
    }

    @WebMethod
    @NotNull
    public ProjectDTO changeProjectStatusById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.changeStatusById(
                session.getUserId(), id, Status.valueOf(status)
        );
    }

    @WebMethod
    @NotNull
    public ProjectDTO changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.changeStatusByIndex(
                session.getUserId(), index, Status.valueOf(status)
        );
    }

    @WebMethod
    @NotNull
    public ProjectDTO changeProjectStatusByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "status", partName = "status")
            @Nullable final String status
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.changeStatusByName(
                session.getUserId(), name, Status.valueOf(status)
        );
    }

    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        projectService.clear(session.getUserId());
    }

    @WebMethod
    public void clearProjects(@WebParam(name = "session", partName = "session")
                              @NotNull final SessionDTO session) {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService()
                .clearProjects(session.getUserId());
    }

    @WebMethod
    @NotNull
    public ProjectDTO completeProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.completeById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public ProjectDTO completeProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.completeByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public ProjectDTO completeProjectByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.completeByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public ProjectDTO deleteProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId")
            @NotNull final String projectId
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectTaskDTOService().deleteProjectById(
                session.getUserId(), projectId
        );
    }

    @WebMethod
    @Nullable
    public List<ProjectDTO> findProjectAll(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    @Nullable
    public List<ProjectDTO> findProjectAllSort(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "sort", partName = "sort")
            @Nullable final String sort
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        @NotNull final Sort sortType = Sort.valueOf(sort);
        return projectService.findAll(
                session.getUserId(), sortType.getComparator()
        );
    }

    @WebMethod
    @Nullable
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.findOneById(session.getUserId(), id);
    }

    @WebMethod
    @Nullable
    public ProjectDTO findProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Nullable
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.findOneByName(session.getUserId(), name);
    }

    @WebMethod
    @Nullable
    public ProjectDTO removeProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @Nullable
    public ProjectDTO removeProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Nullable
    public ProjectDTO removeProjectByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public ProjectDTO startProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public ProjectDTO startProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public ProjectDTO startProjectByName(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public ProjectDTO updateProjectById(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "description", partName = "description")
            @Nullable final String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.updateOneById(
                session.getUserId(), id, name, description
        );
    }

    @WebMethod
    @NotNull
    public ProjectDTO updateProjectByIndex(
            @WebParam(name = "session", partName = "session")
            @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index")
            @Nullable final Integer index,
            @WebParam(name = "name", partName = "name")
            @Nullable final String name,
            @WebParam(name = "description", partName = "description")
            @Nullable final String description
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        return projectService.updateOneByIndex(
                session.getUserId(), index, name, description
        );
    }

}
